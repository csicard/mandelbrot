package cs108;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public final class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Mandelbrot mandelbrot = new Mandelbrot(500);
        ImageView imageView = new ImageView();
        imageView.imageProperty().bind(mandelbrot.imageProperty());

        imageView.setOnMouseClicked(e -> {
            Rectangle frame = mandelbrot.getFrame();
            double iToP = frame.width() / mandelbrot.getWidth();
            double x = e.getX() * iToP;
            double y = frame.height() - e.getY() * iToP;

            if (e.getClickCount() == 1 && e.getButton() == MouseButton.SECONDARY) {
                // Recenter
                double cx = x + frame.minX();
                double cy = y + frame.minY();
                mandelbrot.setFrameCenter(new Point(cx, cy));
            } else if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY) {
                // Zoom in (w/o control) or out (w/ control)
                double scale = (e.isControlDown() ? 2 : 0.5);
                Rectangle newFrame = frame
                        .translatedBy(x, y)
                        .scaledBy(scale)
                        .translatedBy(-x * scale, -y * scale);

                mandelbrot.setFrameCenter(newFrame.center());
                mandelbrot.setFrameWidth(newFrame.width());
            }
        });

        BorderPane mainPane = new BorderPane(imageView);

        mandelbrot.widthProperty().bind(mainPane.widthProperty());
        mandelbrot.heightProperty().bind(mainPane.heightProperty());

        primaryStage.setScene(new Scene(mainPane));
        primaryStage.setTitle("Mandelbrot");
        primaryStage.show();
    }
}
