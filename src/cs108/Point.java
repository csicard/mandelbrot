package cs108;

import java.util.Objects;

public final class Point {
    private final double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double x() { return x; }
    public double y() { return y; }

    public Point translatedBy(double dX, double dY) {
        return new Point(x + dX, y + dY);
    }

    @Override
    public boolean equals(Object thatO) {
        return (thatO instanceof Point)
                && (this.x == ((Point)thatO).x)
                && (this.y == ((Point)thatO).y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return String.format("(%f, %f)", x, y);
    }
}
